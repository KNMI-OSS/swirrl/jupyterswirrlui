module.exports = {
  root: true,
  extends: [
    'airbnb',
    'airbnb-typescript',
    'prettier',
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'plugin:prettier/recommended',
  ],
  plugins: [
    'babel',
    'react',
    'promise',
    'jsx-a11y',
    'react-hooks',
    '@typescript-eslint',
  ],
  ignorePatterns: ['webpack.config.js'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: 'tsconfig.json',
    sourceType: 'module',
    ecmaVersion: 2017,
    ecmaFeatures: {
      jsx: true,
    },
  },
  rules: {
    'react/jsx-curly-newline': 'off',
    'react/jsx-indent': 'off',
    'react/jsx-closing-bracket-location': 'off',
    'react/jsx-closing-tag-location': 'off',
    'no-use-before-define': 'off',
    '@typescript-eslint/no-floating-promises': 'off',
    'import/prefer-default-export': 'off',
    'no-console': 'off',
    'react/function-component-definition': [
      2,
      {
        namedComponents: 'arrow-function',
        unnamedComponents: 'arrow-function',
      },
    ],
  },
};
