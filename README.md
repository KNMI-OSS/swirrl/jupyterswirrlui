# JupyterSwirrlUI

UI elements for swirrl notebook

### Development install

Prerequisites: Install conda. Then run the following commands to
create a conda environment.

```bash
conda create -n swirrlui -c conda-forge python=3.10 jupyterlab
conda activate swirrlui
```

Afterwards you can develop the swirrlui using the following commands.

```bash
# Clone the repo to your local environment
# Change directory to repo's root directory
# Install package in development mode
pip install -e .
# Copy over the static dir (pip should do this for you but it somehow doesn't)
cp -r ./swirrlui/labextension/static $CONDA_PREFIX/share/jupyter/labextensions/swirrlui/
# Run jupyter lab
jupyter lab
```

Make sure you cleanup any build artifacts in the `swirrlui/` directory afterwards.

### Fixing vulnerabilities in dependencies
If `yarn audit` identifies a vulnerability it is typically enough to just delete the
`yarn.lock` file and rerun either the `yarn` or the `jlpm` command. If this is not enough
you can check out the next section for more involved upgrading.

### Upgrading to new jupyterlab version

A lot of files in this repo are not written by us but in stead are obtained from the jupyter
lab cookiecutter example. If jupyterlab upgrades, they usually also release an upraded cookiecutter.
To see how to use cookiecutter see here: https://jupyterlab.readthedocs.io/en/stable/extension/extension_tutorial.html.
If you answer the cookiecutter questions as follows you get a new empty swirrlui extension that you can then copy
the existing content into.
```
kind: server
author_name: swirrl-api
author_email: swirrl-api@knmi.nl
labextension_name: swirrlui
python_name: swirrlui
project_short_description: UI elements for swirrl notebook
has_settings: n
has_binder: n
test: n
repository: https://gitlab.com/KNMI-OSS/swirrl/jupyterswirrlui
```

You can then copy over the following files from this new swirrlui to the existing repo replacing the existing files:

```
tsconfig.json
setup.py
pyproject.toml
package.json
install.json
.prettierignore
.eslintignore
swirrlui/__init.py
style/index.css
style/index.js
jupyter-config/**/*
```

The `tsconfig.json` and `package.json` files require a bit of extra attention afterwards. There are some options in
tsconfig that we overwrote like `noUnusedLocals`. And for `package.json` we need to reinstall all of our dependencies.
The easiest to reinstall all dependencies is to just remove every entry in dependencies and devDependencies. Then save
the file and run the commands below.

```
jlpm add @emotion/react
jlpm add @emotion/styled
jlpm add @jupyterlab/application
jlpm add @jupyterlab/apputils
jlpm add @jupyterlab/coreutils
jlpm add @jupyterlab/services
jlpm add @mui/base
jlpm add @mui/icons-material
jlpm add @mui/material
jlpm add react
jlpm add react-dom
jlpm add uuid

jlpm add --dev @babel/core
jlpm add --dev @jupyterlab/builder
jlpm add --dev @lumino/widgets
jlpm add --dev @types/uuid
jlpm add --dev @typescript-eslint/eslint-plugin
jlpm add --dev @typescript-eslint/parser
jlpm add --dev eslint
jlpm add --dev eslint-config-airbnb
jlpm add --dev eslint-config-airbnb-typescript
jlpm add --dev eslint-config-prettier
jlpm add --dev eslint-plugin-babel
jlpm add --dev eslint-plugin-import
jlpm add --dev eslint-plugin-jsx-a11y
jlpm add --dev eslint-plugin-prettier
jlpm add --dev eslint-plugin-promise
jlpm add --dev eslint-plugin-react
jlpm add --dev eslint-plugin-react-hooks
jlpm add --dev mkdirp
jlpm add --dev npm-run-all
jlpm add --dev prettier
jlpm add --dev rimraf
jlpm add --dev typanion
jlpm add --dev typescript
jlpm add --dev yjs
```

### SWIRRL-API Development deployment

swirrl-api needs the `notebook-images` configmap that can be created by
running `skaffold run` in a clone of this repository. It will build the
docker images from the currently checked out version and publish the
image tags of the created images in the `notebook-images` configmap.

When you are doing development, you generally do not need all types of
notebook. So you can build only the base notebook by executing:

```shell
NAMESPACE=swirrl skaffold build --cache-artifacts=false -b swirrlui-jupyter-notebook
NAMESPACE=swirrl skaffold run
```

When the `build` command has been done and a `k8s/notebook-images.properties` file
exists with the correct content, then it is sufficient to only execut the `run` command.