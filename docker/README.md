# Alternative Jupyter docker images.

NOTE:
> This README was copied verbatim from the `alternative-notebooks`
> subdirectory, now deleted, in the https://gitlab.com/KNMI-OSS/swirrl/swirrl-api
> repository.

In this directory there are some Dockerfiles for jupyter/minimal-notebook
derived docker images that install specific software into the image, so
this does not have to be done by the user through `conda install` or
`pip install` commands.

By default swirrl api now uses the swirrlui image as this image installs the
jupyter lab extension (https://gitlab.com/KNMI-OSS/swirrl/jupyterswirrlui).
The other alternative notebook images use this image as the base image.

The swirrl-api must be compiled with `notebook.docker.image.tag` set to the
image's URL in this projects container registry, or the environment variable
`NOTEBOOK_DOCKER_IMAGE_TAG` needs to be set before starting up swirrl-api.

For example: to use the jupyter notebook with dispel4py pre-installed one
can edit `kubernetes.yaml` before deploying swirrl-api into the k8s cluster:

```yaml
env:
  - name: NOTEBOOK_DOCKER_IMAGE_TAG
    value: registry.gitlab.com/knmi-oss/swirrl/swirrl-api/notebook/dispel4py-jupyter-notebook
```

Or from the command line:

```shell script
NOTEBOOK_DOCKER_IMAGE_TAG=registry.gitlab.com/knmi-oss/swirrl/swirrl-api/notebook/dispel4py-jupyter-notebook \
java -jar target/swirrl-api-1.0.0.jar
```

Or set the environment variable in `docker run`:

```shell script
docker run --rm -p 8080:8080 \
    -e NOTEBOOK_DOCKER_IMAGE_TAG=registry.gitlab.com/knmi-oss/swirrl/swirrl-api/notebook/dispel4py-jupyter-notebook \
    -e KUBECONFIG=/tmp/kube-config/config \
    -e NEO4J_USER=${NEO4J_USER} -e NEO4J_PASSWORD=${NEO4J_PASSWORD} \
    -e NOTEBOOK_URL_ORIGIN=${NOTEBOOK_URL_ORIGIN} \
    -v $HOME/.kube/config:/tmp/kube-config/config \
    -v $HOME/Rdf:/home/swirrl/Rdf:rw \
    --network="host" \
    --name swirrl-api swirrl-api
```

## Currently available images

- swirrlui (DEFAULT): registry.gitlab.com/knmi-oss/swirrl/swirrl-api/notebook/swirrlui-jupyter-notebook
- dispel4py: registry.gitlab.com/knmi-oss/swirrl/swirrl-api/notebook/dispel4py-jupyter-notebook
- icclim: registry.gitlab.com/knmi-oss/swirrl/swirrl-api/notebook/icclim-jupyter-notebook

## Images currently in development

None.

## Building the docker images by hand

This should not be needed, as they are created in the CI/CD pipeline.

> WARNING: The example below will overwrite the images created by the pipeline.

Example assumes you wish to create the `dispel4py` notebook image.

```shell script
cd dispel4py
docker build -t registry.gitlab.com/knmi-oss/swirrl/swirrl-api/notebook/dispel4py-jupyter-notebook .
docker push registry.gitlab.com/knmi-oss/swirrl/swirrl-api/notebook/dispel4py-jupyter-notebook
```
