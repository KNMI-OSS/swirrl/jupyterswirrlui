#!/bin/sh

images="swirrlui-jupyter-notebook dispel4py-jupyter-notebook icclim-jupyter-notebook"
  
for image in ${images} ; do
  echo "Attempt to pull ${CI_REGISTRY_IMAGE}/${image}:${CI_COMMIT_SHORT_SHA}"
  if docker pull ${CI_REGISTRY_IMAGE}/${image}:${CI_COMMIT_SHORT_SHA} ; then
    echo "Success. Tagging ${CI_REGISTRY_IMAGE}/${image}:${CI_COMMIT_REF_SLUG}"
    docker tag ${CI_REGISTRY_IMAGE}/${image}:${CI_COMMIT_SHORT_SHA} ${CI_REGISTRY_IMAGE}/${image}:${CI_COMMIT_REF_SLUG}
    echo "Pushing ${CI_REGISTRY_IMAGE}/${image}:${CI_COMMIT_REF_SLUG}"
    docker push ${CI_REGISTRY_IMAGE}/${image}:${CI_COMMIT_REF_SLUG}
  else
    echo "Failed. Pulling ${CI_REGISTRY_IMAGE}/${image}:${CI_COMMIT_REF_SLUG}"
    docker pull ${CI_REGISTRY_IMAGE}/${image}:${CI_COMMIT_REF_SLUG}
    echo "Tagging ${CI_REGISTRY_IMAGE}/${image}:${CI_COMMIT_SHORT_SHA}"
    docker tag ${CI_REGISTRY_IMAGE}/${image}:${CI_COMMIT_REF_SLUG} ${CI_REGISTRY_IMAGE}/${image}:${CI_COMMIT_SHORT_SHA}
    echo "Pushing ${CI_REGISTRY_IMAGE}/${image}:${CI_COMMIT_SHORT_SHA}"
    docker push ${CI_REGISTRY_IMAGE}/${image}:${CI_COMMIT_SHORT_SHA}
  fi
done