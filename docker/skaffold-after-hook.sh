#!/bin/sh -x

mkdir -p ./k8s

EXTENSION=$1 # swirrlui, dispel4py or icclim
PROPERTIES=./k8s/notebook-images.properties
if [ ! -e ${PROPERTIES} ] ; then
  ## Use appending operator here, in case a parallel build wrote the file after this check
  echo "notebook.docker.image.${EXTENSION}=${SKAFFOLD_IMAGE}" >> ${PROPERTIES}
else
  if grep -q "notebook.docker.image.${EXTENSION}" ${PROPERTIES} ; then
    sed -i -e "s^notebook.docker.image.${EXTENSION}=.*^notebook.docker.image.${EXTENSION}=${SKAFFOLD_IMAGE}^" ${PROPERTIES}
  else
    echo "notebook.docker.image.${EXTENSION}=${SKAFFOLD_IMAGE}" >> ${PROPERTIES}
  fi
fi

## For easier reference, will only be executed when running on a gitlab pipeline:
if [ "${CI_COMMIT_REF_SLUG}" = "master" ] ; then
  docker tag ${SKAFFOLD_IMAGE} ${CI_REGISTRY_IMAGE}/${EXTENSION}-jupyter-notebook:latest
elif [ ! -z "${CI_COMMIT_REF_SLUG}" ] ; then
  docker tag ${SKAFFOLD_IMAGE} ${CI_REGISTRY_IMAGE}/${EXTENSION}-jupyter-notebook:${CI_COMMIT_REF_SLUG}
fi
