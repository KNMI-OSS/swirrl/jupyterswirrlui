#!/bin/sh

mkdir -p ./k8s

PROPERTIES=./k8s/notebook-images.properties
if [ ! -e ${PROPERTIES} ] ; then
  ## Create the properties file from build output
  skaffold build --file-output=build.json --dry-run
  for tag in $(jq -r '.builds[].tag' build.json) ; do
    label=notebook.docker.image.$(echo "${tag}" | sed -rn 's/.*jupyterswirrlui\/(.*)\-jupyter\-notebook.*/\1/p')
    echo "${label}=${tag}" >> ${PROPERTIES}
  done
fi

kubectl --namespace ${NAMESPACE} create configmap notebook-images \
        --from-file ./k8s/notebook-images.properties -o yaml --dry-run=client | kubectl --namespace ${NAMESPACE} apply -f -
