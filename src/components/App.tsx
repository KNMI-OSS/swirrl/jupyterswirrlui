import React, { useEffect } from 'react';
import { URLExt } from '@jupyterlab/coreutils';
import { ServerConnection } from '@jupyterlab/services';

import { ActivityTable } from './widgets/ActivityTable';
import {
  deleteLatestStage,
  getActivities,
  getNotebook,
  Graph,
} from '../utils/swirrlApi';
import { GithubComponent } from './widgets/Github';
import { StatusWidget, STATUS, Status } from './widgets/StatusWidget';
import { SwirrlHeader } from './SwirrlHeader';
import { Login } from './Login';
import { UserProvider, ConfigCtx } from './ApplicationContext';
import { SnapshotWidget } from './widgets/SnapshotWidget';
import { DataStagingWidget } from './widgets/DataStagingWidget';
import { useInterval } from '../utils/useIntervalHook';

const classes = {
  main: {
    fontSize: '1.0rem',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: 400,
    lineHeight: 1.334,
    letterSpacing: '0em',
    backgroundColor: 'var(--jp-layout-color1)',
    color: 'var(--jp-ui-font-color1)',
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    textAlign: 'center',
    minWidth: '420px',
    height: '100%',
    overflowY: 'scroll',
    overflowX: 'hidden',
  },
};

const MINUTE = 60000;

type SettingsType = Record<string, string>;

export async function getSettings(
  init: RequestInit = {},
): Promise<SettingsType> {
  // Make request to Jupyter API
  const settings = ServerConnection.makeSettings();
  const requestUrl = URLExt.join(settings.baseUrl, 'swirrlui/settings');

  const response = await ServerConnection.makeRequest(
    requestUrl,
    init,
    settings,
  );

  const data = (await response.json()) as SettingsType;

  if (!response.ok) {
    throw new ServerConnection.ResponseError(response, data.message);
  }

  return data;
}
const Main: React.FC = (): JSX.Element => {
  const { config, setConfig } = React.useContext(ConfigCtx);
  React.useEffect(() => {
    getSettings()
      .then((data) => {
        const userInfo = {
          provenanceUser: data.provenanceUser,
          userName: data.userName,
          authMode: data.authMode,
          group: data.group,
        };
        setConfig({
          loginEnabled: data.loginEnabled === 'yes',
          swirrlUrl: `${data.swirrlUrl}${
            data.loginEnabled === 'yes' ? '/relay' : ''
          }/swirrl-api/v1.0`,
          openidUrl: `${data.swirrlUrl}/protocol/openid-connect`,
          sessionId: data.sessionId,
          serviceId: data.serviceId,
          userInfoB64: btoa(JSON.stringify(userInfo)),
          jupyterToken: data.token,
          userInfo,
        });
      })
      .catch((reason) => {
        console.error(`Error on GET /swirrlui/settings.\n${reason as string}`);
      });
  }, []);

  const [activityList, setActivities] = React.useState<Graph[]>([]);
  const [lockedRollbackButton, setRollbackButtonLocked] =
    React.useState<boolean>(false);

  const [notebookStatus, setNotebookStatus] = React.useState<Status>(
    STATUS.ACTIVE,
  );

  const refreshNotebookStatus = () =>
    getNotebook(config).then((notebook) =>
      setNotebookStatus(
        notebook ? (notebook.notebookStatus as Status) : STATUS.ERROR,
      ),
    );

  // Only do these once, on mount and when config changes:
  useEffect(() => {
    getActivities(config).then((activities) => setActivities(activities));
  }, [config]);

  useEffect(() => {
    refreshNotebookStatus();
  }, [config]);

  // Update in interval:
  useInterval(() => {
    getActivities(config).then((activities) => setActivities(activities));
  }, 1 * MINUTE);

  useInterval(() => {
    refreshNotebookStatus();
  }, 1 * MINUTE);

  const deleteStage = (): void => {
    refreshNotebookStatus();

    if (notebookStatus === STATUS.ACTIVE) {
      setRollbackButtonLocked(true);
      deleteLatestStage(config);
      setTimeout(() => setRollbackButtonLocked(false), 3000);
    }
  };

  const reloadActivities = (): void => {
    getActivities(config).then((activities) => setActivities(activities));
  };

  return (
    <div style={classes.main as React.CSSProperties}>
      {config && config.loginEnabled && <Login />}
      <SwirrlHeader />
      <StatusWidget notebookStatus={notebookStatus} />
      <GithubComponent />
      <SnapshotWidget
        reloadActivities={reloadActivities}
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        refreshNotebookStatus={refreshNotebookStatus}
        setNotebookStatus={setNotebookStatus}
      />
      <DataStagingWidget
        notebookStatus={notebookStatus}
        lockButton={lockedRollbackButton}
        deleteStage={deleteStage}
      />
      <ActivityTable
        activityList={activityList}
        reloadActivities={reloadActivities}
      />
    </div>
  );
};

export const App: React.FC = () => {
  return (
    <UserProvider>
      <Main />
    </UserProvider>
  );
};
