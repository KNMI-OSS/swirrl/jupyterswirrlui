// Global config settings

/** OPENID configuration. */
export const OPENID = {
  authorization_endpoint: (baseUrl: string) => `${baseUrl}/auth`,
  jwks_uri: (baseUrl: string) => `${baseUrl}/certs`,
  token_endpoint: (baseUrl: string) => `${baseUrl}/token`,
  introspection_endpoint: (baseUrl: string) => `${baseUrl}/token/introspect`,
  userinfo_endpoint: (baseUrl: string) => `${baseUrl}/userinfo`,
  end_session_endpoint: (baseUrl: string) => `${baseUrl}/logout`,
};
