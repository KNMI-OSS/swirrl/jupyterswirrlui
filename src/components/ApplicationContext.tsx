import React from 'react';

type Props = {
  children: React.ReactElement;
};

export type OpenidUser = {
  email: string;
  email_verified: boolean;
  family_name: string;
  given_name: string;
  groups: string[];
  name: string;
  preferred_username: string;
  sub: string;
  registered: boolean;
};

export type UserCtxType = {
  user: OpenidUser;
  setUser: (user: OpenidUser) => void;
};

export const UserCtx = React.createContext({} as UserCtxType);

export const UserProvider = ({ children }: Props): JSX.Element => {
  const [user, setUser] = React.useState<OpenidUser>(null);
  const userObject = React.useMemo(() => ({ user, setUser }), [user, setUser]);
  return <UserCtx.Provider value={userObject}>{children}</UserCtx.Provider>;
};

export type UserInfo = {
  provenanceUser: string;
  userName: string;
  authMode: string;
  group: string;
};

export type Config = {
  loginEnabled: boolean;
  swirrlUrl: string;
  openidUrl: string;
  sessionId: string;
  serviceId: string;
  userInfoB64: string;
  userInfo: UserInfo;
  jupyterToken: string;
};

export type ConfigCtxType = {
  config: Config;
  setConfig: (config: {
    swirrlUrl: string;
    userInfoB64: string;
    userInfo: {
      authMode: string;
      provenanceUser: string;
      userName: string;
      group: string;
    };
    jupyterToken: string;
    loginEnabled: boolean;
    openidUrl: string;
    sessionId: string;
    serviceId: string;
  }) => void;
};

export const ConfigCtx = React.createContext({} as ConfigCtxType);

export const ConfigProvider = ({ children }: Props): JSX.Element => {
  const [config, setConfig] = React.useState<Config>(null);
  const configObject = React.useMemo(
    () => ({ config, setConfig }),
    [config, setConfig],
  );
  return (
    <ConfigCtx.Provider value={configObject}>{children}</ConfigCtx.Provider>
  );
};
