import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import React from 'react';
import { styled } from '@mui/material/styles';
import { Tooltip, IconButton, tooltipClasses } from '@mui/material';

const LightTooltip = styled(Tooltip)(() => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: 'white',
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: 'gray',
    padding: '10px',
    border: '1px solid #e0e0e0',
    fontSize: 16,
  },
}));

export const Info = ({ content }: { content: string }): React.ReactElement => {
  return (
    <LightTooltip title={`${content}`} placement='right'>
      <IconButton
        style={{
          padding: '0px',
          marginTop: '-2px',
          marginLeft: '5px',
        }}
      >
        <InfoOutlinedIcon style={{ color: '#a5a5a5' }} />
      </IconButton>
    </LightTooltip>
  );
};
