import React from 'react';
import { Typography, AppBar } from '@mui/material';

const classes = {
  appBar: {
    backgroundColor: 'var(--jp-layout-color1)',
    color: 'var(--jp-ui-font-color1)',
    textAlign: 'center',
  },
  header: {
    marginTop: '5px',
    backgroundColor: 'var(--jp-layout-color1)',
    color: 'var(--jp-ui-font-color1)',
  },
  image: {
    display: 'inline-block',
    verticalAlign: 'middle',
    marginRight: '20px',
    'mix-blend-mode': 'multiply',
  },
};

export const SwirrlHeader = (): React.ReactElement => {
  return (
    <AppBar position='static' sx={classes.appBar}>
      <Typography variant='h4' sx={classes.header}>
        <div style={classes.image}>
          <img
            alt='Swirrl Logo'
            src='https://gitlab.com/uploads/-/system/project/avatar/17988184/Screenshot_2020-04-09_at_10.37.38.png?width=40'
          />
        </div>
        SWIRRL
      </Typography>
    </AppBar>
  );
};
