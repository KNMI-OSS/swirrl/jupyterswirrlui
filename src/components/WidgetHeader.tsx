import React from 'react';
import { Info } from './Info';

const classes = {
  wrapper: {
    display: 'flex',
    justifyContent: 'center',
    border: '1px solid #d6d6d6',
    padding: '4px',
    backgroundColor: 'var(--jp-layout-color2)',
    margin: '-2px -1px 0 -2px',
    boxShadow:
      '0px 2px 1px -1px rgb(0 0 0 / 10%), 0px 4px 5px 0px rgb(0 0 0 / 7%), 0px 1px 10px 0px rgb(0 0 0 / 6%)',
  },
  title: {
    fontSize: '16px',
    letterSpacing: '0.5px',
    color: 'var(--jp-ui-font-color1)',
  },
  body: {
    padding: '30px 0px',
    color: 'var(--jp-ui-font-color1)',
    marginBottom: '10px',
  },
};

export const WidgetHeader = ({
  title,
  content,
}: {
  title: string;
  content: string;
}): React.ReactElement => {
  return (
    <div style={classes.wrapper}>
      <p style={classes.title}>{`${title}`}</p>
      <Info content={content} />
    </div>
  );
};
export const WidgetContent = ({
  children,
}: {
  children: React.ReactElement;
}): React.ReactElement => {
  return <div style={classes.body}>{children}</div>;
};
