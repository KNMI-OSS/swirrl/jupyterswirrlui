import React, { useEffect } from 'react';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Box,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Button,
  Paper,
} from '@mui/material';
import {
  getActivityById,
  InstalledLibraries,
  getNotebook,
} from '../../utils/swirrlApi';
import {
  OP,
  Change,
  LibCollection,
  getChangeList,
} from '../../utils/parseActivity';
import { ConfigCtx } from '../ApplicationContext';

export type ActivityModalState = {
  activityId: string;
  serviceId: string;
  open: boolean;
};

const classes = {
  tablecontainer: {
    maxHeight: '450px',
  },
  headrow: {
    backgroundColor: 'var(--jp-layout-color2)',
  },
  headcell: {
    fontWeight: 600,
    textAlign: 'center',
    borderBottom: 'var(--jp-border-width) solid var(--jp-border-color2)',
    color: 'var(--jp-ui-font-color1)',
    backgroundColor: 'var(--jp-layout-color2)',
  },
  cell: {
    textAlign: 'center',
    borderBottom: 'var(--jp-border-width) solid var(--jp-border-color2)',
    color: 'var(--jp-ui-font-color1)',
  },
  addspan: {
    fontWeight: 600,
    color: 'green',
  },
  changespan: {
    fontWeight: 600,
    color: 'orange',
  },
  deletespan: {
    fontWeight: 600,
    color: 'red',
  },
  row: {
    minWidth: '500px',
    backgroundColor: 'var(--jp-layout-color1)',
  },
  dialogTitle: {
    color: '#fff',
    backgroundColor: 'var(--jp-brand-color1)',
    fontWeight: 400,
  },
  dialogContent: {
    minWidth: '500px',
    color: 'var(--jp-ui-font-color1)',
    backgroundColor: 'var(--jp-layout-color1)',
  },
  dialogText: {
    color: 'var(--jp-ui-font-color1)',
  },
  dialogFooter: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '1px',
    backgroundColor: 'var(--jp-layout-color2)',
  },
};

type ActivityModal = {
  activityModal: ActivityModalState;
  setRestoreModal: React.Dispatch<React.SetStateAction<ActivityModalState>>;
  setActivityModal: React.Dispatch<React.SetStateAction<ActivityModalState>>;
};

export const ActivityInfoModal = ({
  activityModal,
  setRestoreModal,
  setActivityModal,
}: ActivityModal): React.ReactElement => {
  const { config } = React.useContext(ConfigCtx);
  const [libList, setLibList] = React.useState<InstalledLibraries>(null);
  const [libCollection, setLibCollection] = React.useState<LibCollection>(null);

  const closeState = {
    open: false,
    serviceId: '',
    activityId: '',
  };

  useEffect(() => {
    getNotebook(config).then((notebook) => {
      setLibList(notebook && notebook.installedLibraries);
    });
    getActivityById(activityModal.activityId, config).then((activity) => {
      const libCollectObj =
        activity &&
        activity['prov:generated']['prov:hadMember'].find((member) =>
          member['@type'].includes('swirrl:LibCollection'),
        );
      setLibCollection(libCollectObj && libCollectObj['prov:hadMember']);
    });
  }, []);

  const changeList = getChangeList(libList, libCollection);

  return (
    <Dialog
      open={activityModal.open}
      onClose={(): void => setActivityModal(closeState)}
      aria-labelledby='info-dialog-title'
      aria-describedby='info-dialog-description'
    >
      <DialogTitle id='info-dialog-title' sx={classes.dialogTitle}>
        Library diff
      </DialogTitle>
      <DialogContent sx={classes.dialogContent}>
        {changeList &&
          (changeList.length === 0 ? (
            <DialogContentText
              sx={classes.dialogText}
              id='info-dialog-description'
            >
              Restoring to this activity will not result in any change.
            </DialogContentText>
          ) : (
            <>
              <DialogContentText
                sx={classes.dialogText}
                id='info-dialog-description'
              >
                Restoring to this activity will perform the following actions.
              </DialogContentText>
              <TableContainer component={Paper} sx={classes.tablecontainer}>
                <Table size='small' aria-label='activity info' stickyHeader>
                  <TableHead>
                    <TableRow sx={classes.headrow}>
                      <TableCell sx={classes.headcell}>Library</TableCell>
                      <TableCell sx={classes.headcell}>Version</TableCell>
                      <TableCell sx={classes.headcell}>Action</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {changeList.map((change: Change) => {
                      let spanClass = {};
                      switch (change.op) {
                        case OP.ADD:
                          spanClass = classes.addspan;
                          break;
                        case OP.CHANGE:
                          spanClass = classes.changespan;
                          break;
                        case OP.DELETE:
                          spanClass = classes.deletespan;
                          break;
                        default:
                          spanClass = '';
                      }
                      return (
                        <TableRow key={change.lib} sx={classes.row}>
                          <TableCell sx={classes.cell}>{change.lib}</TableCell>
                          <TableCell sx={classes.cell}>
                            {change.version}
                          </TableCell>
                          <TableCell sx={classes.cell}>
                            <Box component='span' sx={spanClass}>
                              {change.op}&nbsp;
                            </Box>
                            {change.newversion && (
                              <Box component='span' display='inline-block'>
                                {change.newversion}
                              </Box>
                            )}
                            <br />
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
            </>
          ))}
      </DialogContent>
      <DialogActions sx={classes.dialogFooter}>
        <Button
          onClick={(): void => setActivityModal(closeState)}
          color='primary'
        >
          Cancel
        </Button>
        <Button
          onClick={(): void => {
            setRestoreModal({
              ...activityModal,
            });
          }}
          color='primary'
        >
          Restore
        </Button>
      </DialogActions>
    </Dialog>
  );
};
