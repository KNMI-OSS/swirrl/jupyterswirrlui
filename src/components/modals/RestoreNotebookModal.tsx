import React from 'react';
import {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Button,
  LinearProgress,
  Dialog,
} from '@mui/material';
import WarningIcon from '@mui/icons-material/Warning';
import { restoreLibs } from '../../utils/swirrlApi';
import { ConfigCtx } from '../ApplicationContext';

export type RestoreModalState = {
  activityId: string;
  serviceId: string;
  open: boolean;
};

type RestoreModal = {
  setRestoreModal: React.Dispatch<React.SetStateAction<RestoreModalState>>;
  restoreModal: RestoreModalState;
};

const classes = {
  dialogTitle: {
    color: '#fff',
    backgroundColor: '#FF0000',
    fontWeight: 400,
  },
  dialogContent: {
    color: 'var(--jp-ui-font-color1)',
    backgroundColor: 'var(--jp-layout-color1)',
  },
  dialogText: {
    color: 'var(--jp-ui-font-color1)',
  },
  dialogFooter: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '1px',
    backgroundColor: 'var(--jp-layout-color2)',
  },
  link: {
    padding: '10px 0',
    display: 'block',
  },
  highlight: {
    fontWeight: 800,
  },
  icon: {
    color: 'fff',
    verticalAlign: 'text-bottom',
    paddingRight: '15px',
  },
};

export const RestoreNotebookModal = ({
  setRestoreModal,
  restoreModal,
}: RestoreModal): React.ReactElement => {
  const { config } = React.useContext(ConfigCtx);

  const [Spinner, setSpinner] = React.useState<boolean>(false);
  const [buttonsLocked, setButtonsLocked] = React.useState<boolean>(false);

  const closeState = {
    open: false,
    serviceId: '',
    activityId: '',
  };
  return (
    <Dialog
      open={restoreModal.open}
      onClose={(_: object, reason: string) => {
        if (reason !== 'backdropClick') {
          setRestoreModal(closeState);
        }
      }}
      aria-labelledby='alert-dialog-title'
      aria-describedby='alert-dialog-description'
    >
      <DialogTitle id='alert-dialog-title' sx={classes.dialogTitle}>
        <WarningIcon sx={classes.icon} />
        Service downtime incoming
      </DialogTitle>
      <DialogContent sx={classes.dialogContent}>
        <DialogContentText
          sx={classes.dialogText}
          id='alert-dialog-description'
        >
          In order to restore to a previous version of your notebook, Jupyter
          Lab needs to be restarted.
          <span style={classes.highlight}>
            This process can take up to 5 minutes.
          </span>{' '}
          As soon as Jupyter Lab restarts you will be brought to our splash
          screen, which will automatically bring you back once the restart is
          complete.
        </DialogContentText>
        {Spinner && <LinearProgress />}
      </DialogContent>
      <DialogActions sx={classes.dialogFooter}>
        <Button
          onClick={(): void => setRestoreModal(closeState)}
          disabled={buttonsLocked}
          color='primary'
        >
          Cancel
        </Button>
        <Button
          onClick={(): void => {
            restoreLibs(
              restoreModal.serviceId,
              restoreModal.activityId,
              config,
            );
            setSpinner(true);
            setButtonsLocked(true);
            setTimeout(
              () =>
                window.location.assign(
                  `${window.location.href}?token=${config.jupyterToken}&clickedRestore=true`,
                ),
              7000,
            );
          }}
          color='primary'
          disabled={buttonsLocked}
        >
          Agree & Restart
        </Button>
      </DialogActions>
    </Dialog>
  );
};
