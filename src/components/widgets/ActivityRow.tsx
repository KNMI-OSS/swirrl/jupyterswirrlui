import React from 'react';
import {
  TableCell,
  TableRow,
  Typography,
  Button,
  Tooltip,
  Box,
  Link,
} from '@mui/material';
import RestoreIcon from '@mui/icons-material/Restore';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import LinkIcon from '@mui/icons-material/Link';
import {
  getActivityTypeString,
  formatDate,
  parseActivityName,
  restorableActivies,
  Activity,
  ACTIVITIES,
} from '../../utils/parseActivity';
import { ActivityModalState } from '../modals/ActivityInfoModal';
import { Graph, getActivityUrl } from '../../utils/swirrlApi';
import { ConfigCtx } from '../ApplicationContext';

const classes = {
  box: {
    backgroundColor: 'var(--jp-layout-color3)',
    padding: '2px 4px',
    borderRadius: '4px',
    marginRight: '3px',
    marginBottom: '3px',
  },
  topbox: {
    margin: '2px',
    padding: '2px',
  },
  spancell: {
    borderBottom: 'none',
    color: 'var(--jp-ui-font-color1)',
    paddingTop: '0px',
  },
  cell: {
    borderTop: 'var(--jp-border-width) solid var(--jp-border-color2)',
    textAlign: 'center',
    borderBottom: 'none',
    paddingLeft: '0px',
    paddingRight: '0px',
    color: 'var(--jp-ui-font-color1)',
  },
  row: {
    minWidth: '100px',
    backgroundColor: 'var(--jp-layout-color1)',
  },
  pre: {
    borderRadius: '2px',
    border: '1px solid var(--jp-border-color1)',
    padding: '4px 6px',
    margin: '0px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
};

type ActivityRowProps = {
  activity: Graph;
  setActivityModal: React.Dispatch<React.SetStateAction<ActivityModalState>>;
  colSpans: number[];
};

export const ActivityRow = ({
  activity,
  setActivityModal,
  colSpans,
}: ActivityRowProps): React.ReactElement => {
  const { config } = React.useContext(ConfigCtx);
  const activityName = getActivityTypeString(activity['@type']);

  let libList = '';
  let command = '';
  let githubUrl = '';
  let workflowType = '';
  let message = '';
  const messageClass = { ...classes.box, color: 'white' };
  if (activity['@type'].includes(ACTIVITIES.CREATE as string)) {
    libList = activity['prov:plan'] || '';
  } else if (activity['@type'].includes(ACTIVITIES.UPDATE as string)) {
    command = activity['prov:plan'] && activity['prov:plan'].split('\n')[0];
  } else if (activity['@type'].includes(ACTIVITIES.SNAPSHOT as string)) {
    githubUrl = activity['prov:atLocation'] || '';
  } else if (activity['@type'].includes(ACTIVITIES.WORKFLOW as string)) {
    workflowType = activity['prov:plan'] || '';
    message = activity['swirrl:message'] || '';
    if (message.toLowerCase().includes('error')) {
      messageClass.backgroundColor = 'error.main';
    } else {
      messageClass.backgroundColor = 'success.main';
    }
  }

  return (
    <>
      <TableRow sx={classes.row}>
        <TableCell sx={classes.cell} colSpan={colSpans[0]}>
          {parseActivityName(activityName, workflowType)}
          <Link
            href={getActivityUrl(activity['@id'], config)}
            target='_blank'
            rel='noopener'
          >
            <OpenInNewIcon fontSize='inherit' style={{ paddingLeft: 5 }} />
          </Link>
        </TableCell>
        <TableCell sx={classes.cell} colSpan={colSpans[1]}>
          <Tooltip title={formatDate(activity['prov:startedAtTime'])}>
            <div>
              {formatDate(activity['prov:startedAtTime']).substring(0, 16)}
            </div>
          </Tooltip>
        </TableCell>
        <TableCell sx={classes.cell} colSpan={colSpans[2]}>
          {/* eslint-disable-next-line no-nested-ternary */}
          {restorableActivies.includes(activityName as Activity) ? (
            <Button
              color='primary'
              size='small'
              startIcon={<RestoreIcon />}
              onClick={(): void =>
                setActivityModal({
                  open: true,
                  activityId: activity['@id'],
                  serviceId: activity['swirrl:serviceId'],
                })
              }
            >
              Restore
            </Button>
          ) : (activityName as Activity) === ACTIVITIES.SNAPSHOT &&
            githubUrl ? (
            <Link
              href={githubUrl}
              style={{ textDecoration: 'none', color: '#4F60BB' }}
              target='_blank'
            >
              <Button color='primary' size='small' startIcon={<LinkIcon />}>
                Open
              </Button>
            </Link>
          ) : (
            <div />
          )}
        </TableCell>
      </TableRow>
      {libList && (
        <TableRow sx={classes.row}>
          <TableCell
            sx={classes.spancell}
            colSpan={colSpans.reduce((a, b) => a + b)}
          >
            <Box display='flex' flexWrap='wrap' sx={classes.topbox}>
              <Typography variant='body2'>
                Requested libraries:&nbsp;
              </Typography>
              {libList.split('\n').map(
                (lib: string) =>
                  lib && (
                    <Box key={lib} sx={classes.box} p={1}>
                      {lib}
                    </Box>
                  ),
              )}
            </Box>
          </TableCell>
        </TableRow>
      )}
      {command && (
        <TableRow sx={classes.row}>
          <TableCell
            sx={classes.spancell}
            colSpan={colSpans.reduce((a, b) => a + b)}
          >
            <Box display='flex' flexWrap='wrap' sx={classes.topbox}>
              <pre style={classes.pre as React.CSSProperties}>
                <code>{command}</code>
              </pre>
            </Box>
          </TableCell>
        </TableRow>
      )}
      {message && (
        <TableRow sx={classes.row}>
          <TableCell
            sx={classes.spancell}
            colSpan={colSpans.reduce((a, b) => a + b)}
          >
            <Box display='flex' flexWrap='wrap' sx={classes.topbox}>
              <Box sx={messageClass} p={1}>
                {message}
              </Box>
            </Box>
          </TableCell>
        </TableRow>
      )}
    </>
  );
};
