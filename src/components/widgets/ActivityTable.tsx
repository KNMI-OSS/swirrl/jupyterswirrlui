import React from 'react';
import CachedIcon from '@mui/icons-material/Cached';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Button,
  Paper,
} from '@mui/material';
import {
  RestoreNotebookModal,
  RestoreModalState,
} from '../modals/RestoreNotebookModal';
import {
  ActivityInfoModal,
  ActivityModalState,
} from '../modals/ActivityInfoModal';
import { Graph } from '../../utils/swirrlApi';
import { ActivityRow } from './ActivityRow';
import { WidgetHeader, WidgetContent } from '../WidgetHeader';

const classes = {
  tablecontainer: {
    marginTop: '20px',
    marginBottom: '50px',
  },
  container: {
    border: 'solid var(--jp-border-width) var(--jp-border-color2)',
  },
  table: {
    width: 'calc(100% - 1px)',
    borderBottom: 'var(--jp-border-width) solid var(--jp-border-color2)',
    borderCollapse: 'collapse',
    tableLayout: 'fixed',
  },
  headrow: {
    backgroundColor: 'var(--jp-layout-color0)',
    border: '1px solid var(--jp-layout-color2)',
  },
  headcell: {
    fontWeight: 600,
    textAlign: 'center',
    borderBottom: 'none',
    color: 'var(--jp-ui-font-color1)',
  },
};

export type ActivityTableProps = {
  activityList: Graph[];
  reloadActivities: () => void;
};

export const ActivityTable = ({
  activityList,
  reloadActivities,
}: ActivityTableProps): React.ReactElement => {
  const [restoreModal, setRestoreModal] = React.useState<RestoreModalState>({
    activityId: '',
    serviceId: '',
    open: false,
  });

  const [activityModal, setActivityModal] = React.useState<ActivityModalState>({
    activityId: '',
    serviceId: '',
    open: false,
  });

  const sortByDate = (a: Graph, b: Graph): number =>
    // eslint-disable-next-line no-nested-ternary
    Date.parse(a['prov:startedAtTime']) < Date.parse(b['prov:startedAtTime'])
      ? 1
      : Date.parse(a['prov:startedAtTime']) >
        Date.parse(b['prov:startedAtTime'])
      ? -1
      : 0;

  // Because the update commands would make the activitytable grow too wide, we had to fix the table layout
  // This means we have to define the relative width of the columns
  const colSpans = [4, 4, 3];

  return (
    <div style={classes.container}>
      <WidgetHeader
        title='Activities'
        content='Activity log of this notebook. Restore the notebook to a previous
          state. Expand an activity for the list of libraries that will be
          restored.'
      />
      <WidgetContent>
        <>
          <Button
            variant='outlined'
            color='primary'
            size='small'
            startIcon={<CachedIcon />}
            onClick={reloadActivities}
          >
            Load Activities
          </Button>
          <TableContainer
            component={Paper}
            sx={classes.tablecontainer}
            style={{ overflowX: 'hidden' }}
          >
            <Table sx={classes.table} size='small' aria-label='activity table'>
              <TableHead>
                <TableRow sx={classes.headrow}>
                  <TableCell sx={classes.headcell} colSpan={colSpans[0]}>
                    Type
                  </TableCell>
                  <TableCell sx={classes.headcell} colSpan={colSpans[1]}>
                    Created at
                  </TableCell>
                  <TableCell sx={classes.headcell} colSpan={colSpans[2]}>
                    Action
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {activityList &&
                  activityList.sort(sortByDate).map((activity: Graph) => {
                    return (
                      <ActivityRow
                        key={activity['@id']}
                        activity={activity}
                        setActivityModal={setActivityModal}
                        colSpans={colSpans}
                      />
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
          {restoreModal.open && (
            <RestoreNotebookModal
              restoreModal={restoreModal}
              setRestoreModal={setRestoreModal}
            />
          )}
          {activityModal.open && (
            <ActivityInfoModal
              activityModal={activityModal}
              setRestoreModal={setRestoreModal}
              setActivityModal={setActivityModal}
            />
          )}
        </>
      </WidgetContent>
    </div>
  );
};
