import React from 'react';
import { Button } from '@mui/material';
import RestoreIcon from '@mui/icons-material/Restore';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import { WidgetHeader, WidgetContent } from '../WidgetHeader';
import { STATUS } from './StatusWidget';

const classes = {
  container: {
    border: 'solid var(--jp-border-width) var(--jp-border-color2)',
  },
};

export const DataStagingWidget = ({
  lockButton,
  notebookStatus,
  deleteStage,
}: {
  lockButton: boolean;
  notebookStatus: STATUS;
  deleteStage: () => void;
}): JSX.Element => {
  return (
    <div style={classes.container}>
      <WidgetHeader
        title='Data staging'
        content=' Roll back the latest data staging operation to the previous stage. You can compare stages from the build-in file browser of Jupyter Lab'
      />
      <WidgetContent>
        {notebookStatus === STATUS.ACTIVE ? (
          <Button
            variant='outlined'
            color='primary'
            size='small'
            onClick={deleteStage}
            startIcon={<RestoreIcon />}
            disabled={lockButton}
          >
            Roll back
          </Button>
        ) : (
          <Button
            variant='outlined'
            color='primary'
            size='small'
            onClick={null}
            startIcon={<HighlightOffIcon />}
            disabled
          >
            Notebook updating...
          </Button>
        )}
      </WidgetContent>
    </div>
  );
};
