import React from 'react';
import {
  Button,
  Link,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogActions,
  Avatar,
} from '@mui/material';
import { red } from '@mui/material/colors';
import GitHubIcon from '@mui/icons-material/GitHub';
import { getClientId } from '../../utils/swirrlApi';
import { WidgetHeader, WidgetContent } from '../WidgetHeader';
import { login, getAccessToken, getGithubUser } from '../../utils/github';
import { ConfigCtx } from '../ApplicationContext';

const classes = {
  button: {
    color: 'white',
    backgroundColor: red[500],
    '&:hover': {
      backgroundColor: red[700],
    },
  },
  gitContainer: {
    border: 'solid var(--jp-border-width) var(--jp-border-color2)',
    display: 'flex',
    flexDirection: 'column',
  },
  gituserblock: {
    display: 'inline-block',
  },
  gitusercontent: {
    marginRight: '2px',
    marginLeft: '2px',
    marginTop: '5px',
    float: 'left',
    textAlign: 'left',
    height: '40px',
  },
  gitusercompany: {
    fontSize: '0.9rem',
    color: '#586069',
  },
  alerttext: {
    fontSize: '0.9rem',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: 400,
    letterSpacing: '0em',
  },
  text: {
    fontSize: '0.8rem',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: 400,
    letterSpacing: '0em',
    paddingRight: '10px',
    paddingLeft: '10px',
    marginTop: '15px',
  },
};

export type GithubUser = {
  username: string;
  company: string;
  avatar: string;
};

export const GithubComponent: React.FC = (): JSX.Element => {
  const { config } = React.useContext(ConfigCtx);
  const [gitDialog, openGitDialog] = React.useState<boolean>(false);

  const [clientId, setClientId] = React.useState<string>(
    sessionStorage.getItem('clientId'),
  );
  if (!clientId) {
    getClientId(config).then((clientIdRes) => {
      if (!clientIdRes) {
        return;
      }
      setClientId(clientIdRes);
      sessionStorage.setItem('clientId', clientIdRes);
    });
  }

  const [githubUser, setGithubUser] = React.useState<GithubUser>(null);
  getAccessToken(config)
    .then(() => {
      if (!githubUser) {
        return getGithubUser();
      }
      return null;
    })
    .then((res) => {
      if (res) {
        setGithubUser({
          username: res.login,
          company: res.company,
          avatar: res.avatar_url,
        });
      }
    });

  const handleLogin = (): void => {
    openGitDialog(false);
    login();
  };

  return (
    <div style={classes.gitContainer as React.CSSProperties}>
      <WidgetHeader
        title='Github'
        content='Log in to github to use the snapshot feature'
      />
      <WidgetContent>
        <>
          {githubUser ? (
            <div>
              <div style={classes.gituserblock}>
                <Avatar sx={classes.gitusercontent} src={githubUser.avatar} />
                <div style={classes.gitusercontent as React.CSSProperties}>
                  <div>{githubUser.username}</div>
                  <div style={classes.gitusercompany}>{githubUser.company}</div>
                </div>
              </div>
              <div>
                <Button
                  variant='outlined'
                  color='primary'
                  size='small'
                  onClick={(): void => openGitDialog(true)}
                >
                  Switch User
                </Button>
              </div>
              <Dialog
                open={gitDialog}
                onClose={(): void => openGitDialog(false)}
              >
                <DialogContent>
                  <DialogContentText sx={classes.alerttext}>
                    In order to switch users, you must first{' '}
                    <Link
                      href='https://github.com/logout'
                      target='_blank'
                      rel='noopener'
                    >
                      logout from github
                    </Link>
                    . If you do not do this then you will simply be re-logged in
                    to the same account. Before you logout from github please
                    (for your own security) revoke your access tokens using{' '}
                    <Link
                      href={`https://github.com/settings/connections/applications/${clientId}`}
                      target='_blank'
                      rel='noopener'
                    >
                      this link
                    </Link>
                    .
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button
                    variant='outlined'
                    color='primary'
                    size='small'
                    onClick={(): void => openGitDialog(false)}
                  >
                    Cancel
                  </Button>
                  <Button
                    variant='outlined'
                    color='primary'
                    size='small'
                    onClick={handleLogin}
                    startIcon={<GitHubIcon />}
                  >
                    Login
                  </Button>
                </DialogActions>
              </Dialog>
            </div>
          ) : (
            <div>
              <Button
                variant='outlined'
                color='primary'
                size='small'
                startIcon={<GitHubIcon />}
                onClick={(): void => openGitDialog(true)}
              >
                Login
              </Button>
              <Dialog
                open={gitDialog}
                onClose={(): void => openGitDialog(false)}
              >
                <DialogContent>
                  <DialogContentText sx={classes.alerttext}>
                    Please note that any access token we request for you cannot
                    be revoked by us. This means that even though you might have
                    closed your browser or switched users there might still be
                    some valid access tokens for your account. We do not store
                    these access tokens, but anyone who might have intercepted
                    these tokens would retain access to your github account
                    until you revoke this access.
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button
                    variant='contained'
                    color='primary'
                    size='small'
                    sx={classes.button}
                    onClick={(): void => openGitDialog(false)}
                  >
                    Cancel
                  </Button>
                  <Button
                    variant='contained'
                    color='primary'
                    size='small'
                    sx={classes.button}
                    onClick={handleLogin}
                  >
                    Login
                  </Button>
                </DialogActions>
              </Dialog>
            </div>
          )}
          <div style={classes.text}>
            Please review your access using{' '}
            <Link
              href={`https://github.com/settings/connections/applications/${clientId}`}
              target='_blank'
              rel='noopener'
            >
              this link
            </Link>{' '}
            to revoke your access tokens.
          </div>
        </>
      </WidgetContent>
    </div>
  );
};
