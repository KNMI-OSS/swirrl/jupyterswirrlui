import React from 'react';
import { CircularProgress, Button, Input } from '@mui/material';
import FileCopyIcon from '@mui/icons-material/FileCopy';
import WarningIcon from '@mui/icons-material/Warning';
import { WidgetHeader, WidgetContent } from '../WidgetHeader';
import { createSnapshot } from '../../utils/swirrlApi';
import { STATUS } from './StatusWidget';
import { ConfigCtx } from '../ApplicationContext';

const classes = {
  container: {
    border: 'solid var(--jp-border-width) var(--jp-border-color2)',
  },
  snapshotcontainer: {
    border: 'solid var(--jp-border-width) var(--jp-border-color2)',
    padding: '20px 0',
    display: 'flex',
    flexDirection: 'column',
  },
  message: {
    margin: '8px 0 -8px 0',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
};

type SnapshotState = {
  state: string;
  message?: JSX.Element;
};

type SnapshotProps = {
  reloadActivities: () => void;
  refreshNotebookStatus: () => void;
  setNotebookStatus: (status: STATUS) => void;
};

export const SnapshotWidget = ({
  reloadActivities,
  refreshNotebookStatus,
  setNotebookStatus,
}: SnapshotProps): JSX.Element => {
  const { config } = React.useContext(ConfigCtx);
  const [snapshotState, setSnapshotState] = React.useState<SnapshotState>({
    state: 'ready',
  });

  let userMessage = 'snapshot message';

  const handleSnapshot = (): void => {
    setNotebookStatus(STATUS.SNAPSHOT);
    setSnapshotState({
      state: 'busy',
      message: (
        <div style={classes.message}>
          <CircularProgress size={15} />
          <span style={{ marginLeft: 5 }}>Creating snapshot...</span>
        </div>
      ),
    });
    createSnapshot(userMessage, config).then((snapshotURL) => {
      if (!snapshotURL) {
        setNotebookStatus(STATUS.ERROR);
        setSnapshotState({
          state: 'ready',
          message: (
            <div style={{ ...classes.message, color: '#ce3939' }}>
              <WarningIcon />
              <span style={{ marginLeft: 5 }}>Failed to create snapshot.</span>
            </div>
          ),
        });
      } else {
        setSnapshotState({
          state: 'ready',
          message: (
            <div style={classes.message}>
              Successfully created snapshots will show in the Activities list.
            </div>
          ),
        });
        reloadActivities();
        refreshNotebookStatus();
      }
    });
  };
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    userMessage = e.target.value;
  };

  return (
    <div style={classes.container}>
      <WidgetHeader
        title='Create Snapshot'
        content='Create a snapshot of your notebook and save it in your git repository.'
      />
      <WidgetContent>
        <>
          <Input
            inputProps={{ 'aria-label': 'description' }}
            onChange={handleChange}
            placeholder='Snapshot name'
            required
            style={{
              maxWidth: 150,
              backgroundColor: 'var(--jp-layout-color0)',
              color: 'var(--jp-font-color0)',
            }}
          />
          <Button
            variant='outlined'
            color='primary'
            size='small'
            disabled={snapshotState.state !== 'ready'}
            style={{ marginLeft: '15px' }}
            onClick={handleSnapshot}
            startIcon={<FileCopyIcon />}
          >
            Snapshot
          </Button>
          {snapshotState.message}
        </>
      </WidgetContent>
    </div>
  );
};
