import React from 'react';
import { Alert } from '@mui/material';

export type Status =
  | STATUS.ACTIVE
  | STATUS.ERROR
  | STATUS.SNAPSHOT
  | STATUS.WORKFLOW;

export enum STATUS {
  ACTIVE = 'Active',
  ERROR = 'Error',
  SNAPSHOT = 'Snapshot in progress',
  WORKFLOW = 'Workflow in progress',
}

export type NotebookStatus = {
  notebookStatus: Status;
};

export const StatusWidget = ({
  notebookStatus,
}: NotebookStatus): React.ReactElement => {
  switch (notebookStatus) {
    case STATUS.ERROR:
      return (
        <Alert style={{ padding: '0 10px' }} variant='filled' severity='error'>
          Error retrieving notebook status
        </Alert>
      );
    case STATUS.WORKFLOW:
      return (
        <Alert
          style={{ padding: '0 10px' }}
          variant='filled'
          severity='warning'
        >
          Workflow job running
        </Alert>
      );
    case STATUS.SNAPSHOT:
      return (
        <Alert
          style={{ padding: '0 10px' }}
          variant='filled'
          severity='warning'
        >
          Snapshot in progress
        </Alert>
      );
    default:
      return (
        <Alert
          style={{ padding: '0 10px' }}
          variant='filled'
          severity='success'
        >
          Notebook idle
        </Alert>
      );
  }
};
