import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin,
  ILabShell,
} from '@jupyterlab/application';
import React from 'react';
import { ICommandPalette, ReactWidget } from '@jupyterlab/apputils';
import { App } from './components/App';
import { ConfigProvider } from './components/ApplicationContext';

/**
 * Initialization data for the swirrlui extension.
 */
const extension: JupyterFrontEndPlugin<void> = {
  id: 'swirrlui:plugin',
  requires: [ICommandPalette, ILabShell],
  autoStart: true,
  activate: (app: JupyterFrontEnd) => {
    const widget = ReactWidget.create(
      <ConfigProvider>
        <App />
      </ConfigProvider>,
    );

    widget.id = 'swirrl-buttons';
    widget.title.iconClass = 'swirrlicon jp-SideBar-tabIcon';
    widget.title.caption = 'Swirrl Explorer';

    app.shell.add(widget, 'left', { rank: 1200 });
  },
};

export default extension;
