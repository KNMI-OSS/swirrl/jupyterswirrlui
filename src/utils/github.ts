import { getToken } from './swirrlApi';
import { Config } from '../components/ApplicationContext';

/* eslint-disable-next-line camelcase */
type GithubUser = { login: string; company: string; avatar_url: string };

export const login = (): Promise<void> => {
  const clientId = sessionStorage.getItem('clientId');
  if (!clientId) {
    return;
  }
  let redirectUri = window.location.href;
  const labIndex = redirectUri.indexOf('/lab');
  if (labIndex > 0) {
    redirectUri = redirectUri.substring(0, labIndex + 4);
  }
  const scope = 'repo';
  const array = new Uint8Array(30);
  window.crypto.getRandomValues(array);
  let state = '';
  array.forEach((e) => {
    state += (e % 16).toString(16);
  });
  sessionStorage.setItem('githubState', state);
  window.location.href = `https://github.com/login/oauth/authorize?client_id=${clientId}&redirect_uri=${redirectUri}&scope=${scope}&state=${state}`;
};

export const getAccessToken = async (config: Config): Promise<void> => {
  const urlParams = new URLSearchParams(window.location.search);
  const code = urlParams.get('code');
  const state = urlParams.get('state');
  if (code && state === sessionStorage.getItem('githubState')) {
    const tokenRes = await getToken(code, state, config);
    if (!tokenRes) {
      return;
    }
    sessionStorage.setItem('githubToken', tokenRes);
    urlParams.delete('code');
    urlParams.delete('state');
    window.location.search = urlParams.toString();
  }
};

export const getGithubUser = async (): Promise<GithubUser> => {
  const githubToken = sessionStorage.getItem('githubToken');
  if (!githubToken) {
    return null;
  }

  const url = 'https://api.github.com/user';
  const response = (await fetch(url, {
    method: 'GET',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${githubToken}`,
    },
    referrerPolicy: 'no-referrer-when-downgrade',
  }).then((res) => (res.status === 200 ? res.json() : null))) as GithubUser;

  return response;
};
