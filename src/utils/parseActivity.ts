import { InstalledLibraries } from './swirrlApi';

export type Activity =
  | ACTIVITIES.CREATE
  | ACTIVITIES.UPDATE
  | ACTIVITIES.RESTORE
  | ACTIVITIES.SNAPSHOT
  | ACTIVITIES.WORKFLOW
  | ACTIVITIES.ROLLBACK;

export enum ACTIVITIES {
  CREATE = 'swirrl:CreateNotebook',
  RESTORE = 'swirrl:RestoreLibs',
  SNAPSHOT = 'swirrl:CreateSnapshot',
  UPDATE = 'swirrl:UpdateLibs',
  WORKFLOW = 'swirrl:RunWorkflow',
  ROLLBACK = 'swirrl:RollbackWorkflow',
}

export const getActivityTypeString = (activity: string[]): string =>
  activity.find((element: string) => element.includes('swirrl'));

export const formatDate = (dateString: string): string => {
  const date = new Date(dateString);
  const YYYY = date.getFullYear();
  const MM = `${date.getMonth() + 1}`.padStart(2, '0');
  const DD = `${date.getDate()}`.padStart(2, '0');
  const hh = `${date.getHours()}`.padStart(2, '0');
  const mm = `${date.getMinutes()}`.padStart(2, '0');
  const ss = `${date.getSeconds()}`.padStart(2, '0');
  const SSS = `${date.getMilliseconds()}`.padStart(3, '0');

  return `${YYYY}-${MM}-${DD} ${hh}:${mm}:${ss}.${SSS}`;
};

export const parseActivityName = (
  name: string,
  workflowType: string,
): string => {
  switch (name) {
    case ACTIVITIES.CREATE:
      return 'Create';
    case ACTIVITIES.RESTORE:
      return 'Restore';
    case ACTIVITIES.UPDATE:
      return 'Library Update';
    case ACTIVITIES.SNAPSHOT:
      return 'Snapshot';
    case ACTIVITIES.WORKFLOW:
      return `${
        workflowType.charAt(0).toUpperCase() +
        workflowType.slice(1).toLowerCase()
      } WF`;
    case ACTIVITIES.ROLLBACK:
      return 'WF Rollback';
    default:
      return 'n/a';
  }
};

export const restorableActivies = [
  ACTIVITIES.CREATE,
  ACTIVITIES.UPDATE,
  ACTIVITIES.RESTORE,
];

export enum OP {
  ADD = 'Install',
  CHANGE = 'Restore to',
  DELETE = 'Remove',
}

export type Change = {
  op: OP;
  lib: string;
  version: string;
  newversion?: string;
};

export type LibCollection = {
  'swirrl:name': string;
  '@type': string[];
  'swirrl:installationMode': string;
  '@id': string;
  'swirrl:version': string;
}[];

export const getChangeList = (
  libList: InstalledLibraries,
  libCollection: LibCollection,
): Change[] => {
  let changeList: Change[] = null;
  if (libList && libCollection) {
    changeList = [];
    libList.forEach((lib) => {
      const libC = libCollection.find((l) => l['swirrl:name'] === lib.libname);
      if (!libC) {
        changeList.push({
          op: OP.DELETE,
          lib: lib.libname,
          version: lib.libversion,
        });
      } else if (libC['swirrl:version'] !== lib.libversion) {
        changeList.push({
          op: OP.CHANGE,
          lib: lib.libname,
          version: lib.libversion,
          newversion: libC['swirrl:version'],
        });
      }
    });

    libCollection.forEach((libC) => {
      if (
        libC['swirrl:name'] &&
        !libList.some((el) => el.libname === libC['swirrl:name'])
      ) {
        changeList.push({
          op: OP.ADD,
          lib: libC['swirrl:name'],
          version: libC['swirrl:version'],
        });
      }
    });

    changeList.sort((a, b) => {
      if (a.op > b.op) {
        return 1;
      }
      if (b.op > a.op) {
        return -1;
      }
      if (a.lib > b.lib) {
        return 1;
      }
      return -1;
    });
  }
  return changeList;
};
