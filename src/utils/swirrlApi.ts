import { Config } from '../components/ApplicationContext';

type Headers = {
  Accept?: string;
  'Content-Type'?: string;
  Authorization?: string;
  userInfo?: string;
};

type Used = {
  'prov:value': string;
  '@type': string[];
  '@id': string;
};

type Member = {
  '@type': string[];
  '@id': string;
  'prov:hadMember'?: {
    'swirrl:name': string;
    '@type': string[];
    'swirrl:installationMode': string;
    '@id': string;
    'swirrl:version': string;
  }[];
};

type Activity = {
  'swirrl:serviceId': string;
  'prov:used': Used[];
  'swirrl:sessionId': string;
  'swirrl:poolId': string;
  '@id': string;
  '@type': string[];
  '@context': Record<string, string>;
  'prov:generated': {
    '@type': string[];
    '@id': string;
    'prov:hadMember': Member[];
    'prov:atLocation': string;
  };
  'prov:wasAssociatedWith': {
    'prov:agent': { '@type': string[]; '@id': string };
    'prov:plan': {
      '@type': string[];
      '@id': string;
      'prov:atLocation': string;
      'prov:value'?: string;
    };
  }[];
};

export type Graph = {
  'swirrl:serviceId': string;
  'swirrl:sessionId': string;
  '@type': string[];
  person: string;
  '@id': string;
  'prov:endedAtTime': string;
  'prov:startedAtTime': string;
  'prov:plan'?: string;
  'prov:atLocation'?: string;
  'swirrl:message'?: string;
};

export type Notebook = {
  sessionId: string;
  poolId: string;
  id: string;
  serviceURL: string;
  userRequestedLibraries: InstalledLibraries;
};

export type InstalledLibraries = {
  libname: string;
  libversion: string;
  source: string;
}[];

export type ExtendedNotebook = Notebook & {
  notebookStatus: string;
  volumes: {
    name: string;
    size: number;
  }[];
  installedLibraries: {
    libname: string;
    libversion: string;
    source: string;
  }[];
};

export const createSnapshot = async (
  userMessage: string,
  config: Config,
): Promise<string> => {
  if (!config) return null;
  const githubToken = sessionStorage.getItem('githubToken');
  const url = `${config.swirrlUrl}/notebook/${config.serviceId}/snapshot`;
  const payload = JSON.stringify({ userMessage });
  const headers: Headers = {
    'Content-Type': 'application/json',
  };
  if (config.userInfoB64) {
    headers.userInfo = config.userInfoB64;
  }
  if (githubToken) {
    headers.Authorization = `Bearer ${githubToken}`;
  }

  const response = (await fetch(url, {
    method: 'POST',
    mode: 'cors',
    credentials: 'include',
    headers,
    referrerPolicy: 'no-referrer-when-downgrade',
    body: payload,
  }).then((res) => (res.status === 200 ? res.json() : null))) as {
    snapshotURL: string;
  };

  return response && response.snapshotURL;
};

export const getClientId = async (config: Config): Promise<string> => {
  if (!config) return null;
  const url = `${config.swirrlUrl}/github/client_id`;

  const response = (await fetch(url, {
    method: 'GET',
    mode: 'cors',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
    },
    referrerPolicy: 'no-referrer-when-downgrade',
  }).then((res) => (res.status === 200 ? res.json() : null))) as {
    clientId: string;
  };

  return response && response.clientId;
};

export const getToken = async (
  code: string,
  state: string,
  config: Config,
): Promise<string> => {
  if (!config) return null;
  const url = `${config.swirrlUrl}/github/access_token`;
  const payload = JSON.stringify({ code, state });

  const response = (await fetch(url, {
    method: 'POST',
    mode: 'cors',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
    },
    referrerPolicy: 'no-referrer-when-downgrade',
    body: payload,
  }).then((res) => (res.status === 200 ? res.json() : null))) as {
    /* eslint-disable-next-line camelcase */
    access_token: string;
  };

  return response && response.access_token;
};

export const getActivities = async (config: Config): Promise<Graph[]> => {
  if (!config) return null;
  if (!config.serviceId) {
    console.log('Service id could not be determined');
    return null;
  }

  const url =
    `${config.swirrlUrl}/provenance/session/${config.sessionId}/activities?serviceId=` +
    `${config.serviceId}&includeRuns=true&includeAdditionalInfo=true`;
  const response = (await fetch(url, {
    method: 'GET',
    mode: 'cors',
    credentials: 'include',
    headers: {
      Accept: 'application/json',
    },
    referrerPolicy: 'no-referrer-when-downgrade',
  }).then((res) => (res.status === 200 ? res.json() : null))) as {
    '@graph': Graph[];
  };

  return response && response['@graph'];
};

export const getActivityUrl = (activityId: string, config: Config): string =>
  config && `${config.swirrlUrl}/provenance/activity/${activityId}`;

export const getActivityById = async (
  activityId: string,
  config: Config,
): Promise<Activity> => {
  if (!config) return null;
  const url = getActivityUrl(activityId, config);
  const response = (await fetch(url, {
    method: 'GET',
    mode: 'cors',
    credentials: 'include',
    headers: {
      Accept: 'application/json',
    },
    referrerPolicy: 'no-referrer-when-downgrade',
  }).then((res) => (res.status === 200 ? res.json() : null))) as Activity;

  return response;
};

export const restoreLibs = async (
  serviceId: string,
  activityId: string,
  config: Config,
): Promise<Notebook> => {
  if (!config) return null;
  if (!serviceId || !activityId) {
    return null;
  }

  const url = `${config.swirrlUrl}/notebook/${serviceId}/restorelibs/${activityId}`;
  const headers: Headers = {
    Accept: 'application/json',
  };
  if (config.userInfoB64) {
    headers.userInfo = config.userInfoB64;
  }

  const response = (await fetch(url, {
    method: 'PUT',
    mode: 'cors',
    credentials: 'include',
    headers,
    referrerPolicy: 'no-referrer-when-downgrade',
  }).then((res) => (res.status === 200 ? res.json() : null))) as Notebook;

  return response;
};

export const getNotebook = async (
  config: Config,
): Promise<ExtendedNotebook> => {
  if (!config) return null;
  if (!config.serviceId) {
    return null;
  }

  const url = `${config.swirrlUrl}/notebook/${config.serviceId}`;
  const response = (await fetch(url, {
    method: 'GET',
    mode: 'cors',
    credentials: 'include',
    headers: {
      Accept: 'application/json',
    },
    referrerPolicy: 'no-referrer-when-downgrade',
  }).then((res) =>
    res.status === 200 ? res.json() : null,
  )) as ExtendedNotebook;

  return response;
};

export const deleteLatestStage = async (config: Config): Promise<void> => {
  if (!config) return;
  if (!config.sessionId) {
    return;
  }
  const url = `${config.swirrlUrl}/workflow/${config.sessionId}/deletelatest`;
  const headers: Headers = {
    Accept: 'application/json',
  };
  if (config.userInfoB64) {
    headers.userInfo = config.userInfoB64;
  }

  await fetch(url, {
    method: 'DELETE',
    mode: 'cors',
    credentials: 'include',
    headers,
    referrerPolicy: 'no-referrer-when-downgrade',
  }).then((res) => (res.status === 200 ? res.json() : null));
};
