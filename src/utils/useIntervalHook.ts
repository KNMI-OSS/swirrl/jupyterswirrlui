import { useEffect, useRef } from 'react';

export const useInterval = (callback: () => void, delay: number): void => {
  const savedCallback = useRef<() => void | null>();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    const tick = (): void => {
      savedCallback.current();
    };
    if (delay !== null) {
      const id = setInterval(tick, delay);
      return (): void => {
        clearInterval(id);
      };
    }
    return null;
  }, [callback, delay]);
};
