import json
import os, glob

from jupyter_server.base.handlers import APIHandler
from jupyter_server.utils import url_path_join
import tornado

def read_file(filename):
    with open(filename) as file:
        content = file.readline().rstrip('\n')
        return content

def get_env_vars():
    env_var_dir = '/data/env_vars/'
    env_var_files = [f for f in os.listdir(env_var_dir) if os.path.isfile(env_var_dir + f)]
    env_vars = {name: read_file(env_var_dir + name) for name in env_var_files}
    jpserverfilename = glob.glob("/jupyterlab/.local/share/jupyter/runtime/jpserver-*.json")
    try:
        with open (jpserverfilename[0], "r") as jpserverfile:
            jpserver = json.load(jpserverfile)
            env_vars["token"] = jpserver["token"]
    except:
        pass
    return env_vars

class RouteHandler(APIHandler):
    # The following decorator should be present on all verb methods (head, get, post,
    # patch, put, delete, options) to ensure only authorized user can request the
    # Jupyter server
    @tornado.web.authenticated
    def get(self):
        self.finish(json.dumps(get_env_vars()))


def setup_handlers(web_app):
    host_pattern = ".*$"

    base_url = web_app.settings["base_url"]
    route_pattern = url_path_join(base_url, "swirrlui/settings")
    handlers = [(route_pattern, RouteHandler)]
    web_app.add_handlers(host_pattern, handlers)
