import re
import base64

md_file = 'userguide.md'
embedded_md_file = 'Welcome to SWIRRL.md'

def encode_image(match):
    with open(match.group(1), 'rb') as image_file:
        encoded_string = base64.b64encode(image_file.read())
    return 'src="data:image/png;base64,' + encoded_string.decode() + '"'

with open(md_file, 'r') as f:
    text = f.read()

replaced_text = re.sub('src="(\w+/\w+\.png)"', encode_image, text)

with open(embedded_md_file, 'w') as f:
    f.write(replaced_text)
